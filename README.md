# Frontend Mentor - QR code component solution

This is a solution to the [QR code component challenge on Frontend Mentor](https://www.frontendmentor.io/challenges/qr-code-component-iux_sIO_H). Frontend Mentor challenges help you improve your coding skills by building realistic projects. 

## Table of contents

- [Overview](#overview)
  - [Screenshot](#screenshot)
  - [Links](#links)
- [My process](#my-process)
  - [Built with](#built-with)
  - [What I learned](#what-i-learned)
  - [Continued development](#continued-development)
- [Author](#author)
- [Acknowledgments](#acknowledgments)

## Overview

### Screenshot

![](./screenshot.png)

### Links

- Solution URL: [Add solution URL here](https://your-solution-url.com)
- Live Site URL: [Add live site URL here](https://your-live-site-url.com)

## My process

### Built with

- Semantic HTML5 markup
- CSS custom properties
- Flexbox

### What I learned

I used this exercise to practice some stuff and initiate myself on Frontend Mentor.

### Continued development

I want to focus and improve my skills and knowledge on CSS in order to be able to create better User Interfaces.

## Author

- Frontend Mentor - [@Dartheryon](https://www.frontendmentor.io/profile/Dartheryon)


## Acknowledgments

I want to thank my family for all the support they have provided on these hard times. My wife Diana, for being always here giving me the strength and faith i've been losing. My parents Jorge and Cristina, for their help, confidence and support.

My dear sister Diana for being here encouraging me to push harder and keep fighting.
My nephew Camilo for being the one I want to inspire.

